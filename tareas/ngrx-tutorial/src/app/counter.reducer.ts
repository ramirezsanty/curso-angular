import { createReducer, on } from '@ngrx/store';
import * as states from './counter.actions';

export const initialState = 0;

const _counterReducer = createReducer(
    initialState,
    on(
        states.increment, 
        state => state + 1
    ),
    on(
        states.decrement,
        state => state - 1
    ),
    on(
        states.reset,
        state => 0
    ),
    on(
        states.random,
        state => Math.floor(Math.random() * 100)
    ),
    on(
        states.jump,
        (state, { num }) => num
    )
);

export function counterReducer(state, action) {
    return _counterReducer(state, action);
}
