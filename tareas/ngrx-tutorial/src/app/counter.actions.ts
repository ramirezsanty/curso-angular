import { createAction, props } from '@ngrx/store';

export const increment = createAction('[Counter Component] Increment');

export const decrement = createAction('[Counter Component] Decrement');

export const reset = createAction('[Counter Component] Reset');

export const random = createAction('[Counter Component] Random');

export const jump = createAction('[Counter Component] Jump',
    props<{ num: number }>()
);
