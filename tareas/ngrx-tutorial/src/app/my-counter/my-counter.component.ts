import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import * as action from '../counter.actions';

@Component({
  selector: 'app-my-counter',
  templateUrl: './my-counter.component.html',
  styleUrls: ['./my-counter.component.css']
})
export class MyCounterComponent {  
  count$: Observable<number>;

  constructor(
    private store: Store<{ count: number }>
  ) {
    this.count$ = this.store.pipe(select('count'));
  }

  increment() {
    this.store.dispatch(action.increment());
  }

  decrement() {
    this.store.dispatch(action.decrement());
  }

  reset() {
    this.store.dispatch(action.reset());
  }

  random() {
    this.store.dispatch(action.random());
  }

  jump(n: number) {
    this.store.dispatch(action.jump({num: n}));
    return false;
  }

}
