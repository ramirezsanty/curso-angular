import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { Routes, RouterModule } from '@angular/router';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { FormDestinoComponent } from './form-destino/form-destino.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { 
  DestinosState,
  destinosReducer,
  initialDestinosState 
} from './models/destinos-state.model';
import { 
  ActionReducerMap,
  StoreModule as NgRxStoreModule
} from '@ngrx/store';

const routes: Routes = [
  { path: '', redirectTo: 'inicio', pathMatch: 'full' },
  { path: 'inicio', component: ListaDestinosComponent },
  { path: 'destino', component: DestinoDetalleComponent }
];

// redux init
export interface AppState {
  destinos: DestinosState;
}

const reducers: ActionReducerMap<AppState> = {
  destinos: destinosReducer
}

const reducersInitialState = {
  destinos: initialDestinosState()
}
// fin redux init

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, {initialState: reducersInitialState})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
