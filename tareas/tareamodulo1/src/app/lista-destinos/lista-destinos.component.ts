import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from '../models/destinoViaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  destinos: DestinoViaje[];

  constructor() {
    this.destinos = [];
  }

  ngOnInit(): void {
  }

  guardar(destino: string, pais: string): boolean {
    console.log("Nuevo destino: " + destino + ", país: " + pais);
    this.destinos.push(new DestinoViaje(this.destinos.length + 1, destino, pais));
    console.log(this.destinos);
    return false;
  }

}
