export class DestinoViaje {
    id: number;
    nombre: string;
    pais: string;

    constructor(i: number, n: string, p: string) {
        this.id = i;
        this.nombre = n;
        this.pais = p;
    }
}