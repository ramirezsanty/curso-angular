import { DestinoViaje } from './destinoViaje.model';
import { Injectable, Inject, forwardRef } from '@angular/core';
import { AppState, APP_CONFIG, AppConfig, db} from '../app.module';
import { Store } from '@ngrx/store';
import { NuevoDestinoAction, ElegirDestinoAction, QuitarDestinoAction } from './destinos-state.model';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class DestinosApiClient {
    destinos: DestinoViaje[] = [];

    constructor(
        private store: Store<AppState>,
        @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
        private http: HttpClient
    ) { 
        this.store
            .select(state => state.destinos)
            .subscribe((data) => {
                console.log('Destinos sub store');
                console.log(data);
                this.destinos = data.items;
            });

        this.store
            .subscribe((data) => {
                console.log('All store');
                console.log(data);
            })
    }

    add(d: DestinoViaje) {
        const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
        const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers });
        this.http.request(req).subscribe((data: HttpResponse<{}>) => {
            if (data.status === 200) {
                this.store.dispatch(new NuevoDestinoAction(d));
                const myDb = db;
                myDb.destinos.add(d);
                console.log('Todos los destinos de la DB: ');
                myDb.destinos.toArray().then(destinos => console.log(destinos))
            }
        })
    }

    quitar(d: DestinoViaje) {
        this.store.dispatch(new QuitarDestinoAction(d));
    }
    
    elegir(d: DestinoViaje) {
        console.log('DestinosApiClient - add');

        this.store.dispatch(new ElegirDestinoAction(d));
    }

    getById(id: String): DestinoViaje {
        console.log(this.destinos);
        return this.destinos.filter(function(d) { return d.id.toString() === id; })[0];
    }
}
