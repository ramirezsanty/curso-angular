import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from '../../models/destinoViaje.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteUpAction, VoteDownAction, QuitarDestinoAction } from '../../models/destinos-state.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  @HostBinding('attr.class') cssClass = 'col-md-4';
  
  @Input('idx') position: number;
  @Input() destino: DestinoViaje;

  @Output() clicked: EventEmitter<DestinoViaje>;

  constructor(private store: Store<AppState>) {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  seleccionar() {
    console.log('seleccionar...');
    this.clicked.emit(this.destino);

    return false;
  }

  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }

  quitar() {
    this.store.dispatch(new QuitarDestinoAction(this.destino));
    return false;
  }

}
