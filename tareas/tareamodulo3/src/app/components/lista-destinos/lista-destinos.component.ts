import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../../models/destinoViaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [ DestinosApiClient ]
})

export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  destinos;

  constructor(
    private _destinosApiClient: DestinosApiClient,
    private store: Store<AppState>
  ){
    this.onItemAdded = new EventEmitter();
    this.updates = [];

    this.store.select(state => state.destinos.favorito)
      .subscribe(d => {
        if (d != null) {
          this.updates.push('Se ha elegido a ' + d.nombre);
        }
      });
    
    this.store.select(state => state.destinos.items)
      .subscribe(items => this.destinos = items);
  }

  ngOnInit() {}

  guardar(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  quitar(d: DestinoViaje) {
    this.destinosApiClient.quitar(d);
  }

  seleccionar(e: DestinoViaje) {
    this.destinosApiClient.elegir(e);
  }
  
  public get destinosApiClient(): DestinosApiClient {
    return this._destinosApiClient;
  }
  public set destinosApiClient(value: DestinosApiClient) {
    this._destinosApiClient = value;
  }
  
}
