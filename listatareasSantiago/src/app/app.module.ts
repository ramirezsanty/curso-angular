import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AnadirTareaComponent } from './anadir-tarea/anadir-tarea.component';
import { ListarTareaComponent } from './listar-tarea/listar-tarea.component';

@NgModule({
  declarations: [
    AppComponent,
    AnadirTareaComponent,
    ListarTareaComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
